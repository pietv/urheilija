"use strict";

var dbUrl = "mongodb://localhost:27017/athletics";
var Athlete = require('./model/athlete.js');

class Database {
    constructor() {
        this.mongoose = require('mongoose');
        console.log("trying to connect mongodb databse");
        this.mongoose.connect(dbUrl, {
            useUnifiedTopology: true, 
            useNewUrlParser: true
        });

        this.db = this.mongoose.connection;
        this.db.on('error', console.error.bind(console, 'connection error:'));
        this.db.once('open', function() {
            console.log("We're connected!");
        });
    }

    getAll(callback) {
        console.log("get all athletes from mongodb");
        Athlete.find(function(err, athletes) {
            if(err) {
                callback({operation: "failed"});
            } else {
                console.log("got", athletes);
                callback(athletes);
            }
        });
    }

    postOne(f, l, n, b, w, img, sp, st, callback) {
        console.log(f, l, n, b, w, img, sp, st);
        var newAthlete = new Athlete();
        newAthlete.firstname = f;
        newAthlete.lastname = l;
        newAthlete.nickname = n;
        newAthlete.birth = new Date(b);
        newAthlete.weight = Number(w);
        newAthlete.img = img;
        newAthlete.sport = sp;
        newAthlete.stats = st;
        newAthlete.save(function(err, athlete) {
            if(err) {
                callback({operation: "failed"});
                console.log(err);
            } else {
                console.log("posted", athlete);
                callback(athlete);
            }
        })
    }
};

module.exports = Database;