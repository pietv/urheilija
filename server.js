const express = require('express');
const Database = require('./database.js');
const bodyParser = require('body-parser');

const connection = new Database();
const app = express();
const port = 8080;
const host = "127.0.0.1";

var cors = require('cors');

app.use(cors());

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization");

    if (req.method === 'OPTIONS') {
        res.header("Access-Control-Allow-Methods","PUT,POST,PATCH,DELETE,GET");
        return res.status(200).json({});
    }
    next();
});

app.use(express.static(__dirname+'./client'));

app.get('/api/v1/athletes', (req, res) => {
    console.log("GET /api/v1/athletes");
    connection.getAll(function(athletes) {
        res.send(athletes);
    });
});

app.post('/api/v1/athletes', (req, res) => {
    console.log("POST /api/v1/athletes");
    f = req.body.firstname;
    l = req.body.lastname;
    n = req.body.nickname;
    b = req.body.birth;
    w = req.body.weight;
    img = req.body.img;
    sp = req.body.sport;
    st = req.body.stats;
    connection.postOne(f, l, n, b, w, img, sp, st, function(athlete) {
        res.send(athlete);
    });
});


app.listen(port, host, function() {
    console.log("server up and running @ http://" + host + ":" + port);
});