var mongoose = require('mongoose');

var schema = mongoose.Schema({
    firstname : {
        type: String,
        required: true,
        max: 50
    },
    lastname : {
        type: String,
        required: true,
        max: 50
    },
    nickname : {
        type: String,
        required: true,
        max: 15
    },
    birth : {
        type: Date,
        required: true
    },
    weight : {
        type: Number,
        required: true  
    },
    img : {
        type: String,
        required: true
    },

    sport : {
        type: String,
        required: true
    },

    stats : {
        type: String,
        required: true
    },
})

module.exports = mongoose.model('Athlete', schema);