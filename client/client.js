function loadApp() {
    console.log("loading application");
    $("#athlete_table").hide();
    $("h3").hide();

    $.get("http://localhost:8080/api/v1/athletes", function(data) {
        console.log(data);
      })
  
    // Bind AJAX call to the click event of Button #lataa
    $('#lataa').click(function(event) {

        //$("#lataa").hide();
        $("#athlete_table").fadeIn();
        $("h3").show();
        $("h3").fadeOut(1500);
      
      event.preventDefault();
  
      // The server must be bind to localhost (for testing) as we don't have a FQDN or HTTP proxy available
 
      $.get("http://localhost:8080/api/v1/athletes", function(data) {
        console.log(data);
        data.sort(function(a, b) {
            return parseFloat(a.birth) - parseFloat(b.birth);
        });
        var athlete_data ='';
        $('#athlete_table tbody').empty();
        $.each(data, function(key, value){
            athlete_data += '<tr>';
            athlete_data += '<td>'+value.firstname+' "' +value.nickname+ '" ' +value.lastname+'</td>';
            athlete_data += '<td>'+new Date(value.birth).getFullYear()+'</td>';
            athlete_data += '<td>'+value.weight+'</td>';
            athlete_data += '<td><a href="'+value.img+'" target="_blank">Linkki</a></td>';
            athlete_data += '<td>'+value.sport+'</td>';
            athlete_data += '<td>'+value.stats+'</td>';
            athlete_data += '</tr>';
            
        });
        $('#athlete_table').append(athlete_data);
      })
      .done(function( data ) {
        console.log("response from server :", data);

        
        //TODO: esitä vastaanotettu data webbisivulla, eli 
        // rakenna tässä kohtaa taulukko table+th+tr+td-elementein
        // palvelimelta tuleva vastaus on data-muuttujassa JavaScript-oliona
     //   let athletics = document.getElementById('athletes').innerHTML = "<p>" + JSON.stringify(data) + "</p>";
      })
      .fail(function(err) {
        console.log("error");
      })
      .always(function() {
        console.log("finished");
      });
    });
  }