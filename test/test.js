var supertest = require("supertest");
var should = require("should");


var server = supertest.agent("http://localhost:8080");



describe("SAMPLE unit test",function(){



  it("should add athlete",function(done){



    server
    .post('/api/v1/athletes')
    .send({firstname : 'Amanda', lastname : 'Kotaja', nickname: 'AmaKot', weight: '55', img: 'https://www.yleisurheilu.fi/wp-content/uploads/2019/04/FINLAND_ATHLETICS_WHEELCHAIRRACING_KOTAJA_AMANDA-e1555050560858.jpg', sport: 'Pyörätuolikelaus', stats: 'Dolha 2015/1, Lontoo 2017/1, Lyon 2013/2'})
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err,res){
      res.status.should.equal(200);
      done();
    });
  });


});